# THIS FILE IS GENERATED FROM ORANGE-WIDGET-BASE SETUP.PY
short_version = '4.25.1'
version = '4.25.1'
full_version = '4.25.1'
git_revision = '0392606b2a76682fa53193b40da7d6c6ef42d987'
release = True

if not release:
    version = full_version
    short_version += ".dev"
